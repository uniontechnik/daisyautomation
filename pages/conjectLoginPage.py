import logging
import sys

from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.support.expected_conditions import presence_of_element_located
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.common.by import By
import time
from resources.daisyAutomationConfiguration import *


delay = 10
userNameInput = "//input[@id='UserName']"
passwordInput = "//input[contains(@id,'Password')]"
loginSubmit = "//input[@type='submit']"

def openConjectFm():
    logging.info("INFO - Current function: " + str(__name__) + ".openConjectFm")
    service = Service(executable_path=getPathToChromeDriver())
    conjectDriver = webdriver.Chrome(service=service)

    try:
        conjectDriver.get(getConjectFmUrl())
        conjectDriver.maximize_window()
        logging.info("INFO - Successfully opened URL: " + str(getConjectFmUrl()))
        return conjectDriver
    except Exception as e:
        logging.error("CRITICAL - Could not open URL: " + str(getConjectFmUrl()))
        logging.error("CRITICAL - Exception: " + str(e))
        return False

def loginToConjectFm(conjectDriver):
    logging.info("INFO - Current function: " + str(__name__) + ".loginToConjectFm")
    try:
        WebDriverWait(conjectDriver, delay).until(presence_of_element_located((By.XPATH, userNameInput))).send_keys(getConjectFmUser())
        logging.info("INFO - Successfully entered Username")
    except Exception as e:
        logging.error("CRITICAL - Could not enter Username")
        logging.error("CRITICAL - Exception: " + str(e))
        return False
    try:
        WebDriverWait(conjectDriver, delay).until(presence_of_element_located((By.XPATH, passwordInput))).send_keys(getConjectFmPassword())
        logging.info("INFO - Successfully entered Password")
    except Exception as e:
        logging.error("CRITICAL - Could not enter Password")
        logging.error("CRITICAL - Exception: " + str(e))
        return False
    try:
        WebDriverWait(conjectDriver, delay).until(presence_of_element_located((By.XPATH, loginSubmit))).click()
        logging.info("INFO - Successfully completed login")
    except Exception as e:
        logging.error("CRITICAL - Could not complete login")
        logging.error("CRITICAL - Exception: " + str(e))
        return False
    return True