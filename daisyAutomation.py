from steps.conjectSteps import *
from steps.outlookSteps import *
from datetime import date
import logging
from actions.daisyAutomationMailActions import *

today = date.today()
current_date = today.strftime("%Y%m%d")
logFile = "daisyAutomation-" + current_date + ".log"

logging.basicConfig(filename=logFile, encoding='utf-8', level=logging.INFO, format='%(asctime)s %(message)s', datefmt='%d.%m.%Y %I:%M:%S')

mailData = []

while True:
    logging.info("INFO - #### BEGINNING PROCESS RUN ####")
    #*** Process - log into outlook
    outlookDriver = openOutlookMain()
    if outlookDriver == "couldNotOpenOutlook" or outlookDriver == "couldNotCompleteLoginToOutlook" or outlookDriver == "cannotOpenOutlook":
        #outlookDriver.close()
        time.sleep(30)
        outlookDriver = openOutlookMain()
        if outlookDriver == "couldNotOpenOutlook" or outlookDriver == "couldNotCompleteLoginToOutlook" or outlookDriver == "cannotOpenOutlook":
            sendAlertMail(outlookDriver)
            sys.exit()
        else:
            pass
    else:
        pass

    # *** Process - switch to temp folder
    result = changeFolder(outlookDriver, "temp")
    if result is True:
        pass
    else:
        time.sleep(30)
        result = changeFolder(outlookDriver, "temp")
        if result is True:
            pass
        else:
            sendAlertMail(result + ": temp")
            sys.exit()


    # *** Process - are there mails to process
    while mailsToProcess(outlookDriver) == "mailsToProcess":

        # *** Process - open first mail
        # *** Process - is mail to process??
        evaluateMailToProcessReturn, messageTitle, outlookDriver = evaluateMailToProcess(outlookDriver)

        # *** Process - get mail data
        if evaluateMailToProcessReturn == "toProcess":
            mailData = getMailData(outlookDriver)

            if "couldNotExtract" in mailData:
                time.sleep(10)
                mailData = getMailData(outlookDriver)
            else:
                pass

            # *** Process - enter fields in conject
            caseNumber = str()
            enterServiceInterruptionReturn, conjectDriver = enterServiceInteruptionCase(mailData)
            if enterServiceInterruptionReturn == "couldNotOpenConject" or enterServiceInterruptionReturn == "couldNotLogIntoConject":
                sendAlertMail(enterServiceInterruptionReturn)
                sys.exit()
            elif "SR-T-" in enterServiceInterruptionReturn:
                caseNumber = enterServiceInterruptionReturn
                # *** Process - move to daisy processed
                moveMailTo(outlookDriver, "Daisy bearbeitet")
            else:
                sendNotificationMail(enterServiceInterruptionReturn, messageTitle)
                # *** Process - move to daisy not processed
                moveMailTo(outlookDriver, "Daisy nicht bearbeitet")

            # if caseNumber:
            #     checkSuccessfulSaveReturn = checkSuccessfullSave(caseNumber, conjectDriver)
            #     if checkSuccessfulSaveReturn == "failToNavigateConject" or checkSuccessfulSaveReturn == "helpdeskIncidentNotSaved":
            #         sendNotificationMail(checkSuccessfulSaveReturn, messageTitle)
            #         # *** Process - move to daisy not processed
            #         moveMailTo(outlookDriver, "Daisy nicht bearbeitet")
            #     else:
            #         # *** Process - move to daisy processed
            #         moveMailTo(outlookDriver, "Daisy bearbeitet")
            # else:
            #     # *** Process - move to daisy not processed
            #     moveMailTo(outlookDriver, "Daisy nicht bearbeitet")

            closeConjectDriver(conjectDriver)

        else:
            # *** Process - log not to process
            sendNotificationMail(evaluateMailToProcessReturn, messageTitle)
            # *** Process - move to daisy not processed
            moveMailTo(outlookDriver, "Daisy nicht bearbeitet")

    if mailsToProcess(outlookDriver) == "noMailsToProcess":
        # *** Process - switch to Inbox folder
        result = changeFolder(outlookDriver, "Inbox")
        if result is True:
            pass
        else:
            time.sleep(30)
            result = changeFolder(outlookDriver, "Inbox")
            if result is True:
                pass
            else:
                sendAlertMail(result + ": Inbox")
                sys.exit()
        if mailsToProcess(outlookDriver) == "mailsToProcess":
            # *** Process - move to daisy not processed
            moveResult = moveMailTo(outlookDriver, "temp")
            if moveResult is True:
                pass
            else:
                time.sleep(30)
                moveResult = moveMailTo(outlookDriver, "temp")
                if moveResult is True:
                    pass
                else:
                    sendAlertMail(result + ": temp")
                    sys.exit()
            # *** Process - switch to temp folder
            result = changeFolder(outlookDriver, "temp")
            if result is True:
                pass
            else:
                time.sleep(30)
                result = changeFolder(outlookDriver, "temp")
                if result is True:
                    pass
                else:
                    sendAlertMail(result + ": temp")
                    sys.exit()


    closeOutlookDriver(outlookDriver)


    logging.info("INFO - #### ENDING PROCESS RUN ####")
    time.sleep(10)





