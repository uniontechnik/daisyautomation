import time

from pages.outlookLoginPage import *
from pages.outlookMainPage import *
from pages.outlookMessagePage import *
from actions.outlookMessageActions import *
from actions.outlookContextMenuActions import *
import logging


def openOutlookMain():
    logging.info("INFO - Current function: " + str(__name__) + ".openOutlookMain")
    openOutlookReturn = ""
    try:
        openOutlookReturn = openOutlook()
        if openOutlookReturn:
            outlookDriver = openOutlookReturn
        else:
            openOutlookReturn = "couldNotOpenOutlook"
        if outlookLogin(outlookDriver):
            pass
        else:
            openOutlookReturn = "couldNotCompleteLoginToOutlook"
            outlookDriver.close()
        return(openOutlookReturn)
    except Exception as e:
        logging.error("CRITICAL - could not open outlook driver")
        logging.error("CRITICAL - Exception: " + str(e))
        openOutlookReturn = "cannotOpenOutlook"
        return(openOutlookReturn)


def evaluateMailToProcess(outlookDriver):
    logging.info("INFO - Current function: " + str(__name__) + ".evaluateMailToProcess")
    messageTitle= "NoMessage"
    if openFirstMail(outlookDriver):
        if getMessageName(outlookDriver):
            messageTitle = getMessageName(outlookDriver)
            toProcessFurtherInd = useMessageToCreateIncident(messageTitle)
            match toProcessFurtherInd:
                case "notToProcess":
                    evaluateMailToProcessReturn = "noMatchDoNotProcess"
                case "toProcess":
                    evaluateMailToProcessReturn = "toProcess"
                case "couldNotProcessEmailTitle":
                    evaluateMailToProcessReturn = "couldNotProcessEmailTitle"
        else:
            evaluateMailToProcessReturn = "couldNotGetMessageTitle"
    else:
        evaluateMailToProcessReturn = "noMailsToProcess"

    return(evaluateMailToProcessReturn, messageTitle, outlookDriver)

def getMailData(outlookDriver):
    logging.info("INFO - Current function: " + str(__name__) + ".getMailData")
    getMailDataReturn = ""
    if getReporterFromIncidentMail(outlookDriver):
        reporter = getReporterFromIncidentMail(outlookDriver)
    else:
        getMailDataReturn = "couldNotExtractReporter"
    if getReporterTelephoneNumberFromIncidentMail(outlookDriver):
        reporterTelephoneNumber = getReporterTelephoneNumberFromIncidentMail(outlookDriver)
    else:
        getMailDataReturn = "couldNotExtractReporterTelephoneNumber"
    if getObnFromIncidentMail(outlookDriver):
        obnFromMail = getObnFromIncidentMail(outlookDriver)
        obn = parseObn(obnFromMail)
    else:
        getMailDataReturn = "couldNotExtractObn"
    if getDescriptionFromIncidentMail(outlookDriver):
        description = getDescriptionFromIncidentMail(outlookDriver)
    else:
        getMailDataReturn = "couldNotExtractDescription"
    if getDiagnosisCodeFromIncidentMail(outlookDriver):
        diagnosisCode = getDiagnosisCodeFromIncidentMail(outlookDriver)
    else:
        getMailDataReturn = "couldNotExtractDiagnosisCode"
    if getWorkOrderFromIncidentMail(outlookDriver):
        workOrderLine = getWorkOrderFromIncidentMail(outlookDriver)
        workOrder = parseWorkOrderNumber(workOrderLine)
    else:
        getMailDataReturn = "couldNotExtractWorkOrderLine"
    if getCompleteUntilDateFromIncidentMail(outlookDriver):
        complUntilDateFromMail = getCompleteUntilDateFromIncidentMail(outlookDriver)
        completeUntilDate = transformCompleteUntilDate(complUntilDateFromMail)
    else:
        getMailDataReturn = "couldNotExtractCompleteUntilDate"

    if getMailDataReturn == "":
        mailData = [reporter, reporterTelephoneNumber, obn, description, diagnosisCode, workOrder, completeUntilDate]
        return(mailData)
    else:
        return (getMailDataReturn)

def closeOutlookDriver(outlookDriver):
    logging.info("INFO - Current function: " + str(__name__) + ".closeOutlookDriver")
    try:
        outlookDriver.close()
        logging.info("INFO - successfully closed outlook driver")
        return True
    except Exception as e:
        logging.error("CRITICAL - could not close outlook driver")
        logging.error("CRITICAL - Exception: " + str(e))
        return False

def moveMailTo(outlookDriver, folder):
    logging.info("INFO - Current function: " + str(__name__) + ".moveMailTo")
    logging.info("INFO - Argument: " + str(folder))
    contextMenuReturn = openContextMenu(outlookDriver)
    if contextMenuReturn:
        moveReturn = moveTo(outlookDriver, folder)
        if moveReturn:
            return True
        else:
            return("couldNotMoveToFolder")
    else:
        return("couldNotOpenContextMenu")

def changeFolder(outlookDriver, folder):
    logging.info("INFO - Current function: " + str(__name__) + ".changeFolder")
    logging.info("INFO - Argument: " + str(folder))
    changeFolderReturn = switchToFolder(outlookDriver, folder)
    if changeFolderReturn:
        return True
    else:
        time.sleep(30)
        changeFolderReturn = switchToFolder(outlookDriver, folder)
        if changeFolderReturn:
            return True
        else:
            pass
        return ("couldNotChangeFolder")

def mailsToProcess(outlookDriver):
    logging.info("INFO - Current function: " + str(__name__) + ".mailsToProcess")
    if isAMailToProcessFound(outlookDriver):
        return("mailsToProcess")
    else:
        return("noMailsToProcess")