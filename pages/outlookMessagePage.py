import logging
import sys

from selenium import webdriver
from selenium.webdriver import Keys
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.support.expected_conditions import presence_of_element_located
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.common.by import By
import time
from resources.daisyAutomationConfiguration import *

delay = 10

mailTitleText = "//div[contains(@class,'allowTextSelection')]//span"
reporterText = "//span[contains(text(),'VERANTWORTLICHER')]/parent::p/parent::td/following-sibling::td//span"
reporterTelephoneNumberText = "//strong[contains(text(),'Tankstellen Technik')]/parent::p/parent::td/parent::tr/following-sibling::tr//span[contains(text(),'TELEFONNR')]/parent::p/parent::td/following-sibling::td//span"
obnText = "//span[contains(text(),'TANKSTELLENNR')]/parent::p/parent::td/following-sibling::td//span"
descriptionText = "//span[contains(text(),'BESCHREIBUNG')]/parent::p/parent::td/following-sibling::td//span"
diagnosisCodeText = "//span[contains(text(),'DIAGNOSECODE')]/parent::p/parent::td/following-sibling::td//span"
workOrderText = "//span[contains(text(),'AUFTRAGSNUMMER')]/parent::p/parent::td/following-sibling::td//span"
completeUntilDateText = "//strong[contains(text(),'ERLEDIGEN')]/parent::span/parent::p/parent::td/following-sibling::td//span"

def getMessageName(outlookDriver):
    logging.info("INFO - Current function: " + str(__name__) + ".getMessageName")
    try:
        element = WebDriverWait(outlookDriver, delay).until(presence_of_element_located((By.XPATH, mailTitleText)))
        elementTitle = element.text
        logging.info("INFO - successfully retrieved message title from Email")
        return(elementTitle)
    except Exception as e:
        logging.error("ERROR - could not retrieve message title from Email")
        logging.error("ERROR - Exception: " + str(e))
        return False

def getReporterFromIncidentMail(outlookDriver):
    logging.info("INFO - Current function: " + str(__name__) + ".getReporterFromIncidentMail")
    try:
        element = WebDriverWait(outlookDriver, delay).until(presence_of_element_located((By.XPATH, reporterText)))
        reporter = element.text
        logging.info("INFO - successfully retrieved reporter from Email")
        return (reporter)
    except Exception as e:
        logging.error("ERROR - could not retrieve reporter from Email")
        logging.error("ERROR - Exception: " + str(e))
        return False

def getReporterTelephoneNumberFromIncidentMail(outlookDriver):
    logging.info("INFO - Current function: " + str(__name__) + ".getReporterTelephoneNumberFromIncidentMail")
    try:
        element = WebDriverWait(outlookDriver, delay).until(presence_of_element_located((By.XPATH, reporterTelephoneNumberText)))
        reporter = element.text
        logging.info("INFO - successfully retrieved reporter telephone number from Email")
        return (reporter)
    except Exception as e:
        logging.error("ERROR - could not retrieve reporter telephone number from Email")
        logging.error("ERROR - Exception: " + str(e))
        return False

def getObnFromIncidentMail(outlookDriver):
    logging.info("INFO - Current function: " + str(__name__) + ".getObnFromIncidentMail")
    try:
        element = WebDriverWait(outlookDriver, delay).until(presence_of_element_located((By.XPATH, obnText)))
        reporter = element.text
        logging.info("INFO - successfully retrieved OBN from Email")
        return (reporter)
    except Exception as e:
        logging.error("ERROR - could not retrieve OBN from Email")
        logging.error("ERROR - Exception: " + str(e))
        return False

def getDescriptionFromIncidentMail(outlookDriver):
    logging.info("INFO - Current function: " + str(__name__) + ".getDescriptionFromIncidentMail")
    try:
        element = WebDriverWait(outlookDriver, delay).until(presence_of_element_located((By.XPATH, descriptionText)))
        reporter = element.text
        logging.info("INFO - successfully retrieved description from Email")
        return (reporter)
    except Exception as e:
        logging.error("ERROR - could not retrieve description from Email")
        logging.error("ERROR - Exception: " + str(e))
        return False

def getDiagnosisCodeFromIncidentMail(outlookDriver):
    logging.info("INFO - Current function: " + str(__name__) + ".getDiagnosisCodeFromIncidentMail")
    try:
        element = WebDriverWait(outlookDriver, delay).until(presence_of_element_located((By.XPATH, diagnosisCodeText)))
        reporter = element.text
        logging.info("INFO - successfully retrieved diagnosis code from Email")
        return (reporter)
    except Exception as e:
        logging.error("ERROR - could not retrieve diagnosis code from Email")
        logging.error("ERROR - Exception: " + str(e))
        return False

def getWorkOrderFromIncidentMail(outlookDriver):
    logging.info("INFO - Current function: " + str(__name__) + ".getWorkOrderFromIncidentMail")
    try:
        element = WebDriverWait(outlookDriver, delay).until(presence_of_element_located((By.XPATH, workOrderText)))
        reporter = element.text
        logging.info("INFO - successfully retrieved work order from Email")
        return (reporter)
    except Exception as e:
        logging.error("ERROR - could not retrieve work order from Email")
        logging.error("ERROR - Exception: " + str(e))
        return False

def getCompleteUntilDateFromIncidentMail(outlookDriver):
    logging.info("INFO - Current function: " + str(__name__) + ".getCompleteUntilDateFromIncidentMail")
    try:
        element = WebDriverWait(outlookDriver, delay).until(presence_of_element_located((By.XPATH, completeUntilDateText)))
        completeUntilDate = element.text
        logging.info("INFO - successfully retrieved complete until date from Email")
        return (completeUntilDate)
    except Exception as e:
        logging.error("ERROR - could not retrieve complete until date from Email")
        logging.error("ERROR - Exception: " + str(e))
        return False