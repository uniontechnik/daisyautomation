import json

jsonFile = 'C:/automation/resources/daisyAutomation.json'

def getPathToChromeDriver():
    with open(jsonFile) as json_data:
        data_dict = json.load(json_data)
        return data_dict['pathToChromeDriver']

def getConjectFmUrl():
    with open(jsonFile) as json_data:
        data_dict = json.load(json_data)
        return data_dict['conjectFmUrl']

def getConjectFmUser():
    with open(jsonFile) as json_data:
        data_dict = json.load(json_data)
        return data_dict['conjectFmUser']

def getConjectFmPassword():
    with open(jsonFile) as json_data:
        data_dict = json.load(json_data)
        return data_dict['conjectFmPassword']

def getConjectUtAreaInput():
    with open(jsonFile) as json_data:
        data_dict = json.load(json_data)
        return data_dict['conjectUtAreaInput']

def getConjectResponsibleDepartment():
    with open(jsonFile) as json_data:
        data_dict = json.load(json_data)
        return data_dict['conjectResponsibleDepartment']

def getConjectRevenueType():
    with open(jsonFile) as json_data:
        data_dict = json.load(json_data)
        return data_dict['conjectRevenueType']

def getOutlookUrl():
    with open(jsonFile) as json_data:
        data_dict = json.load(json_data)
        return data_dict['outlookUrl']

def getOutlookUser():
    with open(jsonFile) as json_data:
        data_dict = json.load(json_data)
        return data_dict['outlookUser']

def getOutlookPassword():
    with open(jsonFile) as json_data:
        data_dict = json.load(json_data)
        return data_dict['outlookPassword']

def getOutlookTitleMatchStrings():
    with open(jsonFile) as json_data:
        data_dict = json.load(json_data)
        return data_dict['outlookTitleMatchStrings']

def getEmailSenderEmail():
    with open(jsonFile) as json_data:
        data_dict = json.load(json_data)
        return data_dict['emailSenderEmail']

def getEmailSenderPassword():
    with open(jsonFile) as json_data:
        data_dict = json.load(json_data)
        return data_dict['emailSenderPassword']

def getEmailSenderSmtp():
    with open(jsonFile) as json_data:
        data_dict = json.load(json_data)
        return data_dict['emailSenderSmtp']

def getEmailSenderPort():
    with open(jsonFile) as json_data:
        data_dict = json.load(json_data)
        return data_dict['emailSenderPort']

def getEmailReceiverList():
    with open(jsonFile) as json_data:
        data_dict = json.load(json_data)
        return data_dict['emailReceiverList']

def getEmailCcReceiver():
    with open(jsonFile) as json_data:
        data_dict = json.load(json_data)
        return data_dict['emailCcReceiver']

def getEmailMainReceiver():
    with open(jsonFile) as json_data:
        data_dict = json.load(json_data)
        return data_dict['emailMainReceiver']