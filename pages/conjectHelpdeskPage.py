import logging

from selenium import webdriver
from selenium.webdriver import Keys
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.support.expected_conditions import presence_of_element_located
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.common.by import By
import time
from resources.daisyAutomationConfiguration import *
delay = 10

reporterInput = "//input[@attid='Requester']"
reporterTelephoneInput = "//input[@attid='Telephone']"
openObnFormLink = "//div[contains(@listref,'OBN')]"
formIframeObn = "//iframe[@id='framepopup' and contains(@src,'OBN')]"
formIframeInputObn = "//table[contains(@id,'HeaderTable')]//tr[contains(@id,'FilterRow')]/td[1]//input"
closeObnFormButton = "//span[contains( @class,'closethick')]/parent::button[@title='Close']"
openUtAreaFormLink = "//div[contains(@listref,'UTArea')]"
formIframeUtArea = "//iframe[@id='framepopup' and contains(@src,'UTArea')]"
formIframeInputUtArea = "//input[contains(@class,'dxeEditArea')]"
openRespDeptFormLink ="//div[contains(@listref,'Department')]"
formIframeRespDept = "//iframe[@id='framepopup' and contains(@src,'Department')]"
formIframeInputRespDept = "//input[contains(@class,'dxeEditArea')]"
descriptionInput = "//textarea[@attid='Description']"
openDiagCodeFormLink ="//div[contains(@listref,'Diagnosis')]"
formIframeDiagCode = "//iframe[@id='framepopup' and contains(@src,'Diagnosis')]"
formIframeInputDiagCode = "(//input[contains(@class,'dxeEditArea')])[1]"
closeDiagCodeFormButton = "//span[contains(text(),'Diagnose')]/following-sibling::button"
orderNumberInput = "//td[@title='Auftragsnr.']//following-sibling::td//input[@type='text']"
openRevTypeFormLink ="//div[contains(@listref,'RevenueType')]"
formIframeRevType = "//iframe[@id='framepopup' and contains(@src,'RevenueType')]"
formIframeInputRevType = "(//input[contains(@class,'dxeEditArea')])[1]"
completeUntilDateInput = "//input[@attid='ToDoUntil']"
saveButton = "//input[@type='button' and @value='Speichern']"
saveStatusText = "//div[@class='toast-item toast-type-success']"

def insertReporter(conjectDriver, reporter):
    logging.info("INFO - Current function: " + str(__name__) + ".insertReporter")
    logging.info("INFO - Argument reporter: " + str(reporter))
    try:
        WebDriverWait(conjectDriver, delay).until(presence_of_element_located((By.XPATH, reporterInput))).send_keys(reporter)
        logging.info("INFO - Successfully entered reporter: " + str(reporter))
        return True
    except Exception as e:
        logging.error("ERROR - Could not enter reporter: " + str(reporter))
        logging.error("ERROR - Exception: " + str(e))
        return False

def insertReporterTelephone(conjectDriver, reporterTelephone):
    logging.info("INFO - Current function: " + str(__name__) + ".insertReporterTelephone")
    logging.info("INFO - Argument reporterTelephone: " + str(reporterTelephone))
    try:
        WebDriverWait(conjectDriver, delay).until(presence_of_element_located((By.XPATH, reporterTelephoneInput))).send_keys(reporterTelephone)
        logging.info("INFO - Successfully entered reporter telephone number: " + str(reporterTelephone))
        return True
    except Exception as e:
        logging.error("ERROR - Could not enter reporter telephone number: " + str(reporterTelephone))
        logging.error("ERROR - Exception: " + str(e))
        return False

def openObnForm(conjectDriver):
    logging.info("INFO - Current function: " + str(__name__) + ".openObnForm")
    try:
        WebDriverWait(conjectDriver, delay).until(presence_of_element_located((By.XPATH, openObnFormLink))).click()
        logging.info("INFO - Successfully opened the OBN form")
        return True
    except Exception as e:
        logging.error("ERROR - Could not open the OBN form")
        logging.error("ERROR - Exception: " + str(e))
        return False

def searchAndSelectObn(conjectDriver, obn):
    logging.info("INFO - Current function: " + str(__name__) + ".searchAndSelectObn")
    logging.info("INFO - Argument obn: " + str(obn))
    try:
        iframe = WebDriverWait(conjectDriver, delay).until(presence_of_element_located((By.XPATH, formIframeObn)))
        conjectDriver.switch_to.frame(iframe)
        WebDriverWait(conjectDriver, delay).until(presence_of_element_located((By.XPATH, formIframeInputObn))).send_keys(obn)
        WebDriverWait(conjectDriver, delay).until(presence_of_element_located((By.XPATH, formIframeInputObn))).send_keys(Keys.RETURN)
        try:
            foundRowXpath = "//div[@title='" + obn + "']/parent::td/parent::tr"
            WebDriverWait(conjectDriver, delay).until(presence_of_element_located((By.XPATH, foundRowXpath))).click()
        except Exception as e:
            conjectDriver.switch_to.default_content()
            WebDriverWait(conjectDriver, delay).until(presence_of_element_located((By.XPATH, closeObnFormButton))).click()
            return("obnNotFound")
        conjectDriver.switch_to.default_content()
        logging.info("INFO - Successfully searched for and selected the OBN: " + str(obn))
        return True
    except Exception as e:
        logging.error("ERROR - Could not search and select the OBN: " + str(obn))
        logging.error("ERROR - Exception: " + str(e))
        return False

def openUtAreaForm(conjectDriver):
    logging.info("INFO - Current function: " + str(__name__) + ".openUtAreaForm")
    try:
        WebDriverWait(conjectDriver, delay).until(presence_of_element_located((By.XPATH, openUtAreaFormLink))).click()
        logging.info("INFO - Successfully opened the UT Area form")
        return True
    except Exception as e:
        logging.error("ERROR - Could not open the UT Area form")
        logging.error("ERROR - Exception: " + str(e))
        return False

def searchAndSelectUtArea(conjectDriver):
    logging.info("INFO - Current function: " + str(__name__) + ".searchAndSelectUtArea")
    try:
        iframe = WebDriverWait(conjectDriver, delay).until(presence_of_element_located((By.XPATH, formIframeUtArea)))
        conjectDriver.switch_to.frame(iframe)
        WebDriverWait(conjectDriver, delay).until(presence_of_element_located((By.XPATH, formIframeInputUtArea))).send_keys(getConjectUtAreaInput())
        WebDriverWait(conjectDriver, delay).until(presence_of_element_located((By.XPATH, formIframeInputUtArea))).send_keys(Keys.RETURN)
        foundRowXpath = "//div[@title='" + getConjectUtAreaInput() + "']/parent::td/parent::tr"
        try:
            WebDriverWait(conjectDriver, delay).until(presence_of_element_located((By.XPATH, foundRowXpath))).click()
        except Exception as sere:
            logging.warning("WARN - Handling a stale element reference exception")
            logging.warning("WARN - Exception: " + str(sere))
            WebDriverWait(conjectDriver, delay).until(presence_of_element_located((By.XPATH, foundRowXpath))).click()
        conjectDriver.switch_to.default_content()
        logging.info("INFO - Successfully searched for and selected the UT Area: " + str(getConjectUtAreaInput()))
        return True
    except Exception as e:
        logging.error("ERROR - Could not search and select the UT Area: " + str(getConjectUtAreaInput()))
        logging.error("ERROR - Exception: " + str(e))
        return False

def openResponsibleDepartmentForm(conjectDriver):
    logging.info("INFO - Current function: " + str(__name__) + ".openResponsibleDepartmentForm")
    try:
        WebDriverWait(conjectDriver, delay).until(presence_of_element_located((By.XPATH, openRespDeptFormLink))).click()
        logging.info("INFO - Successfully opened the responsible department form")
        return True
    except Exception as e:
        logging.error("ERROR - Could not open responsible department form")
        logging.error("ERROR - Exception: " + str(e))
        return False

def searchAndSelectResponsibleDepartment(conjectDriver):
    logging.info("INFO - Current function: " + str(__name__) + ".searchAndSelectResponsibleDepartment")
    try:
        iframe = WebDriverWait(conjectDriver, delay).until(presence_of_element_located((By.XPATH, formIframeRespDept)))
        conjectDriver.switch_to.frame(iframe)
        WebDriverWait(conjectDriver, delay).until(presence_of_element_located((By.XPATH, formIframeInputRespDept))).send_keys(getConjectResponsibleDepartment())
        WebDriverWait(conjectDriver, delay).until(presence_of_element_located((By.XPATH, formIframeInputRespDept))).send_keys(Keys.RETURN)
        foundRowXpath = "//div[@title='" + getConjectResponsibleDepartment() + "']/parent::td/parent::tr"
        try:
            WebDriverWait(conjectDriver, delay).until(presence_of_element_located((By.XPATH, foundRowXpath))).click()
        except Exception as sere:
            logging.warning("WARN - Handling a stale element reference exception")
            logging.warning("WARN - Exception: " + str(sere))
            WebDriverWait(conjectDriver, delay).until(presence_of_element_located((By.XPATH, foundRowXpath))).click()
        conjectDriver.switch_to.default_content()
        logging.info("INFO - Successfully searched for and selected the responsible department: " + str(getConjectResponsibleDepartment()))
        return True
    except Exception as e:
        logging.error("ERROR - Could not search and select the responsible department: " + str(getConjectResponsibleDepartment()))
        logging.error("ERROR - Exception: " + str(e))
        return False

def insertDescription(conjectDriver, description):
    logging.info("INFO - Current function: " + str(__name__) + ".insertDescription")
    logging.info("INFO - Argument description: " + str(description))
    try:
        WebDriverWait(conjectDriver, delay).until(presence_of_element_located((By.XPATH, descriptionInput))).send_keys(description)
        logging.info("INFO - Successfully entered description: " + str(description))
        return True
    except Exception as e:
        logging.error("ERROR - Could not enter description: " + str(description))
        logging.error("ERROR - Exception: " + str(e))
        return False

def openDiagnosisCodeForm(conjectDriver):
    logging.info("INFO - Current function: " + str(__name__) + ".openDiagnosisCode")
    try:
        WebDriverWait(conjectDriver, delay).until(presence_of_element_located((By.XPATH, openDiagCodeFormLink))).click()
        logging.info("INFO - Successfully opened the diagnosis code form")
        return True
    except Exception as e:
        logging.error("ERROR - Could not open diagnosis code form")
        logging.error("ERROR - Exception: " + str(e))
        return False

def searchAndSelectDiagnosisCode(conjectDriver, diagnosisCode):
    logging.info("INFO - Current function: " + str(__name__) + ".searchAndSelectDiagnosisCode")
    try:
        iframe = WebDriverWait(conjectDriver, delay).until(presence_of_element_located((By.XPATH, formIframeDiagCode)))
        conjectDriver.switch_to.frame(iframe)
        WebDriverWait(conjectDriver, delay).until(presence_of_element_located((By.XPATH, formIframeInputDiagCode))).send_keys(diagnosisCode)
        WebDriverWait(conjectDriver, delay).until(presence_of_element_located((By.XPATH, formIframeInputDiagCode))).send_keys(Keys.RETURN)
        try:
            try:
                foundRowXpath = "//div[@title='" + diagnosisCode + "']/parent::td/parent::tr"
                WebDriverWait(conjectDriver, delay).until(presence_of_element_located((By.XPATH, foundRowXpath))).click()
            except Exception as e:
                conjectDriver.switch_to.default_content()
                WebDriverWait(conjectDriver, delay).until(presence_of_element_located((By.XPATH, closeDiagCodeFormButton))).click()
                return ("diagnosisCodeNotFound")
        except Exception as sere:
            logging.warning("WARN - Handling a stale element reference exception")
            logging.warning("WARN - Exception: " + str(sere))
            try:
                foundRowXpath = "//div[@title='" + diagnosisCode + "']/parent::td/parent::tr"
                WebDriverWait(conjectDriver, delay).until(presence_of_element_located((By.XPATH, foundRowXpath))).click()
            except Exception as e:
                print("HERE")
                conjectDriver.switch_to.default_content()
                WebDriverWait(conjectDriver, delay).until(presence_of_element_located((By.XPATH, closeDiagCodeFormButton))).click()
                return ("diagnosisCodeNotFound")
        conjectDriver.switch_to.default_content()
        logging.info("INFO - Successfully searched for and selected the diagnosis code: " + str(diagnosisCode))
        return True
    except Exception as e:
        logging.error("ERROR - Could not search and select the diagnosis code: " + str(diagnosisCode))
        logging.error("ERROR - Exception: " + str(e))
        return False

def insertOrderNumber(conjectDriver, orderNumber):
    logging.info("INFO - Current function: " + str(__name__) + ".insertOrderNumber")
    logging.info("INFO - Argument order number: " + str(orderNumber))
    try:
        WebDriverWait(conjectDriver, delay).until(presence_of_element_located((By.XPATH, orderNumberInput))).send_keys(orderNumber)
        logging.info("INFO - Successfully entered order number: " + str(orderNumber))
        return True
    except Exception as e:
        logging.error("ERROR - Could not enter order number: " + str(orderNumber))
        logging.error("ERROR - Exception: " + str(e))
        return False

def openRevenueTypeForm(conjectDriver):
    logging.info("INFO - Current function: " + str(__name__) + ".openRevenueTypeForm")
    try:
        WebDriverWait(conjectDriver, delay).until(presence_of_element_located((By.XPATH, openRevTypeFormLink))).click()
        logging.info("INFO - Successfully opened the revenue type form")
        return True
    except Exception as e:
        logging.error("ERROR - Could not open revenue type form")
        logging.error("ERROR - Exception: " + str(e))
        return False

def searchAndSelectRevenueType(conjectDriver):
    logging.info("INFO - Current function: " + str(__name__) + ".searchAndSelectRevenueType")
    try:
        iframe = WebDriverWait(conjectDriver, delay).until(presence_of_element_located((By.XPATH, formIframeRevType)))
        conjectDriver.switch_to.frame(iframe)
        WebDriverWait(conjectDriver, delay).until(presence_of_element_located((By.XPATH, formIframeInputRevType))).send_keys(getConjectRevenueType())
        WebDriverWait(conjectDriver, delay).until(presence_of_element_located((By.XPATH, formIframeInputRevType))).send_keys(Keys.RETURN)
        foundRowXpath = "//div[@title='" + getConjectRevenueType() + "']/parent::td/parent::tr"
        try:
            WebDriverWait(conjectDriver, delay).until(presence_of_element_located((By.XPATH, foundRowXpath))).click()
        except Exception as sere:
            logging.warning("WARN - Handling a stale element reference exception")
            logging.warning("WARN - Exception: " + str(sere))
            WebDriverWait(conjectDriver, delay).until(presence_of_element_located((By.XPATH, foundRowXpath))).click()
        conjectDriver.switch_to.default_content()
        logging.info("INFO - Successfully searched for and selected the revenue type: " + str(getConjectRevenueType()))
        return True
    except Exception as e:
        logging.error("ERROR - Could not search and select the revenue type: " + str(getConjectRevenueType()))
        logging.error("ERROR - Exception: " + str(e))
        return False

def insertCompleteUntilDate(conjectDriver, complUntilDate):
    logging.info("INFO - Current function: " + str(__name__) + ".insertCompleteUntilDate")
    logging.info("INFO - Argument complete until date: " + str(complUntilDate))
    try:
        WebDriverWait(conjectDriver, delay).until(presence_of_element_located((By.XPATH, completeUntilDateInput))).send_keys(complUntilDate)
        logging.info("INFO - Successfully entered complete until date: " + str(complUntilDate))
        return True
    except Exception as e:
        logging.error("ERROR - Could not enter complete until date: " + str(complUntilDate))
        logging.error("ERROR - Exception: " + str(e))
        return False

def clickSaveButton(conjectDriver):
    logging.info("INFO - Current function: " + str(__name__) + ".clickSaveButton")
    try:
        WebDriverWait(conjectDriver, delay).until(presence_of_element_located((By.XPATH, saveButton))).click()
        logging.info("INFO - Successfully clicked save button")
        return True
    except Exception as e:
        logging.error("ERROR - Could not click save button")
        logging.error("ERROR - Exception: " + str(e))
        return False

def getOrderSaveStatus(driver):
    logging.info("INFO - Current function: " + str(__name__) + ".getOrderSaveStatus")
    try:
        statusTextRaw = WebDriverWait(driver, delay).until(presence_of_element_located((By.XPATH, saveStatusText))).text
        caseNumber = statusTextRaw.split('"')[1]
        logging.info("INFO - Order was saved correctly / without error - case number: " + caseNumber)
        return(caseNumber)
    except Exception as e:
        logging.error("ERROR - Could not correctly save order")
        logging.error("ERROR - Exception: " + str(e))
        return False