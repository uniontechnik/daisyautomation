from pages.conjectLoginPage import *
from pages.conjectLandingPage import *
from pages.conjectHelpdeskPage import *
from pages.conjectAllIncidentsPage import *

def enterServiceInteruptionCase(mailData):
    logging.info("INFO - Current function: " + str(__name__) + ".enterServiceInteruptionCase")
    failToInsertDataErrorCnt = 0

    enterServiceInterruptionReturn = openConjectFm()
    if enterServiceInterruptionReturn:
        conjectDriver = enterServiceInterruptionReturn
    else:
        enterServiceInterruptionReturn = "couldNotOpenConject"
    if loginToConjectFm(conjectDriver):
        pass
    else:
        enterServiceInterruptionReturn = "couldNotLogIntoConject"
    if expandMainMenu(conjectDriver):
        pass
    else:
        enterServiceInterruptionReturn = "failToNavigateConject"
    if selectMenuItem(conjectDriver, "Helpdesk"):
        pass
    else:
        enterServiceInterruptionReturn = "failToNavigateConject"
    if insertReporter(conjectDriver, mailData[0]):
        pass
    else:
        enterServiceInterruptionReturn = "couldNotInsertReporter"
        failToInsertDataErrorCnt += 1
    if insertReporterTelephone(conjectDriver, mailData[1]):
        pass
    else:
        enterServiceInterruptionReturn = "couldNotInsertReporterTelefoneNumber"
        failToInsertDataErrorCnt += 1
    if openObnForm(conjectDriver):
        pass
    else:
        enterServiceInterruptionReturn = "couldNotOpenObnForm"
        failToInsertDataErrorCnt += 1
    if searchAndSelectObn(conjectDriver, mailData[2]):
        message = searchAndSelectObn(conjectDriver, mailData[2])
        if message == "obnNotFound":
            enterServiceInterruptionReturn = message
        else:
            pass
        pass
    else:
        enterServiceInterruptionReturn = "couldNotSearchAndSelectObn"
        failToInsertDataErrorCnt += 1
    if openUtAreaForm(conjectDriver):
        pass
    else:
        enterServiceInterruptionReturn = "couldNotOpenUtAreaForm"
        failToInsertDataErrorCnt += 1
    if searchAndSelectUtArea(conjectDriver):
        pass
    else:
        enterServiceInterruptionReturn = "couldNotSearchAndSelectUtArea"
        failToInsertDataErrorCnt += 1
    if openResponsibleDepartmentForm(conjectDriver):
        pass
    else:
        enterServiceInterruptionReturn = "couldNotOpenResponsibleDepartmentForm"
        failToInsertDataErrorCnt += 1
    if searchAndSelectResponsibleDepartment(conjectDriver):
        pass
    else:
        enterServiceInterruptionReturn = "couldNotSearchAndSelectResponsibleDepartment"
        failToInsertDataErrorCnt += 1
    if insertDescription(conjectDriver, mailData[3]):
        pass
    else:
        enterServiceInterruptionReturn = "couldNotEnterADescription"
        failToInsertDataErrorCnt += 1
    if openDiagnosisCodeForm(conjectDriver):
        pass
    else:
        enterServiceInterruptionReturn = "couldNotOpenDiagnosisCodeForm"
        failToInsertDataErrorCnt += 1
    if searchAndSelectDiagnosisCode(conjectDriver, mailData[4]):
        message = searchAndSelectDiagnosisCode(conjectDriver, mailData[4])
        if message == "diagnosisCodeNotFound":
            enterServiceInterruptionReturn = message
        else:
            pass
        pass
    else:
        enterServiceInterruptionReturn = "couldNotSearchAndSelectDiagnosisCode"
        failToInsertDataErrorCnt += 1
    if insertOrderNumber(conjectDriver, mailData[5]):
        pass
    else:
        enterServiceInterruptionReturn = "couldNotEnterOrderNumber"
        failToInsertDataErrorCnt += 1
    if openRevenueTypeForm(conjectDriver):
        pass
    else:
        enterServiceInterruptionReturn = "couldNotOpenRevenueTypeForm"
        failToInsertDataErrorCnt += 1
    if searchAndSelectRevenueType(conjectDriver):
        pass
    else:
        enterServiceInterruptionReturn = "couldNotSearchAndSelectRevenueType"
        failToInsertDataErrorCnt += 1
    if insertCompleteUntilDate(conjectDriver, mailData[6]):
        pass
    else:
        enterServiceInterruptionReturn = "couldNotEnterCompleteUntilDate"
        failToInsertDataErrorCnt += 1
    if clickSaveButton(conjectDriver):
        pass
    else:
        enterServiceInterruptionReturn = "couldNotClickSave"
        failToInsertDataErrorCnt += 1
    if failToInsertDataErrorCnt == 0:
        caseNumber = getOrderSaveStatus(conjectDriver)
        if caseNumber:
            enterServiceInterruptionReturn = caseNumber
        else:
            enterServiceInterruptionReturn = "couldNotRetrieveCaseNumber"
    else:
        pass
    return(enterServiceInterruptionReturn, conjectDriver)

def checkSuccessfullSave(caseNumber, conjectDriver):
    logging.info("INFO - Current function: " + str(__name__) + ".enterServiceInteruptionCase")
    logging.info("INFO - Argument case number: " + str(caseNumber))
    if expandMainMenu(conjectDriver):
        pass
    else:
        checkSuccessfulSaveReturn = "failToNavigateConject"
    if selectMenuItem(conjectDriver, "Alle Meldungen"):
        pass
    else:
        checkSuccessfulSaveReturn = "failToNavigateConject"
    if searchForIncident(caseNumber, conjectDriver):
        checkSuccessfulSaveReturn = "success"
    else:
        checkSuccessfulSaveReturn = "helpdeskIncidentNotSaved"
    return(checkSuccessfulSaveReturn)

def closeConjectDriver(conjectDriver):
    logging.info("INFO - Current function: " + str(__name__) + ".closeConjectDriver")
    try:
        conjectDriver.close()
        logging.info("INFO - successfully closed conject driver")
        return True
    except Exception as e:
        logging.error("CRITICAL - could not close conject driver")
        logging.error("CRITICAL - Exception: " + str(e))
        return False