import logging

from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.support.expected_conditions import presence_of_element_located
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.common.by import By
import time
from resources.daisyAutomationConfiguration import *

delay = 10

def searchForIncident(caseNumber, conjectDriver):
    logging.info("INFO - Current function: " + str(__name__) + ".searchForIncident")
    xpath = "//div[@title='" + caseNumber + "']"
    try:
        if (WebDriverWait(conjectDriver, delay).until(presence_of_element_located((By.XPATH, xpath)))):
            logging.info("INFO - Found case number: " + str(caseNumber))
            return True
        else:
            print("ERROR - Case number not saved successfully: " + str(caseNumber))
            return False
    except Exception as e:
        logging.error("ERROR - Could not case number: " + str(caseNumber))
        logging.error("ERROR - Exception: " + str(e))
        return False