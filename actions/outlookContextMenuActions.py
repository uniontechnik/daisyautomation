import logging

from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.support.expected_conditions import presence_of_element_located
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.common.action_chains import ActionChains
import time

delay = 10
contextMenuMove = "//button[@name='Move']"

def moveTo(outlookDriver, folder):
    logging.info("INFO - Current function: " + str(__name__) + ".moveTo")
    logging.info("INFO - Argument: " + str(folder))
    try:
        elementToHover =  WebDriverWait(outlookDriver, delay).until(presence_of_element_located((By.XPATH, contextMenuMove)))
        hover = ActionChains(outlookDriver).move_to_element(elementToHover)
        hover.perform()
        time.sleep(4)

        folderXpath = "//div[contains(@class,'Callout')]//span[contains(text(),'" + folder + "')]"
        #folderToClick = outlookDriver.find_element(By.XPATH, folderXpath)
        folderToClick = WebDriverWait(outlookDriver, delay).until(presence_of_element_located((By.XPATH, folderXpath)))
        folderToClick.click()
        time.sleep(6)
        logging.info("INFO - Successfully moved to " + str(folder))
        return True
    except Exception as e:
        logging.error("ERROR - Could not move to " + str(folder))
        logging.error("ERROR - Exception:  " + str(e))
        return False
