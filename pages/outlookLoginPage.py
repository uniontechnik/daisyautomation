import logging
import sys

from selenium import webdriver
from selenium.webdriver import Keys
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.support.expected_conditions import presence_of_element_located
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.common.by import By
import time
from resources.daisyAutomationConfiguration import *


delay = 30
userNameInput = "//input[@type='email']"
nextAfterUserNameButton = "//input[@type='submit']"
passwordInput = "//input[@type='password']"
loginSubmit = "//input[@data-report-event='Signin_Submit']"
stayLoggedInScreen = "//div[@role='heading']"

def openOutlook():
    logging.info("INFO - Current function: " + str(__name__) + ".openOutlook")
    service = Service(executable_path=getPathToChromeDriver())
    outlookDriver = webdriver.Chrome(service=service)

    try:
        outlookDriver.get(getOutlookUrl())
        outlookDriver.maximize_window()
        logging.info("INFO - Successfully opened URL: " + str(getOutlookUrl()))
        return outlookDriver
    except Exception as e:
        logging.error("ERROR - Could not open URL: " + str(getOutlookUrl()))
        logging.error("ERROR - Exception: " + str(e))
        return False

def outlookLogin(outlookDriver):
    logging.info("INFO - Current function: " + str(__name__) + ".outlookLogin")
    try:
        WebDriverWait(outlookDriver, delay).until(presence_of_element_located((By.XPATH, userNameInput))).send_keys(getOutlookUser())
        logging.info("INFO - Successfully entered Username")
    except Exception as e:
        logging.error("CRITICAL - Could not enter Username")
        logging.error("CRITICAL - Exception: " + str(e))
        return False
    try:
        WebDriverWait(outlookDriver, delay).until(presence_of_element_located((By.XPATH, nextAfterUserNameButton)))
        time.sleep(2)
        WebDriverWait(outlookDriver, delay).until(presence_of_element_located((By.XPATH, nextAfterUserNameButton))).click()
        WebDriverWait(outlookDriver, delay).until(presence_of_element_located((By.XPATH, passwordInput)))
        logging.info("INFO - Successfully clicked next")
    except Exception as e:
        logging.error("CRITICAL - Could not click next")
        logging.error("CRITICAL - Exception: " + str(e))
        return False
    try:
        WebDriverWait(outlookDriver, delay).until(presence_of_element_located((By.XPATH, passwordInput))).send_keys(getOutlookPassword())
        logging.info("INFO - Successfully entered Password")
    except Exception as e:
        logging.error("CRITICAL - Could not enter Password")
        logging.error("CRITICAL - Exception: " + str(e))
        return False
    try:
        WebDriverWait(outlookDriver, delay).until(presence_of_element_located((By.XPATH, loginSubmit)))
        time.sleep(2)
        WebDriverWait(outlookDriver, delay).until(presence_of_element_located((By.XPATH, loginSubmit))).click()
        time.sleep(2)
        stayloggedInElement = WebDriverWait(outlookDriver, 5).until(presence_of_element_located((By.XPATH, stayLoggedInScreen))).text
        if "ngemeldet" in stayloggedInElement:
            logging.info("INFO - Successfully completed login")
        else:
            return False
    except Exception as e:
        logging.error("CRITICAL - Could not complete login")
        logging.error("CRITICAL - Exception: " + str(e))
        return False
    try:
        WebDriverWait(outlookDriver, delay).until(presence_of_element_located((By.XPATH, loginSubmit))).click()
        logging.info("INFO - Successfully clicked stay logged in")
    except Exception as e:
        logging.error("CRITICAL - Could not click stay logged in")
        logging.error("CRITICAL - Exception: " + str(e))
        return False
    return True

