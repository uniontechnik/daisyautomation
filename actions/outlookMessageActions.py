import logging

from resources.daisyAutomationConfiguration import *
import re

def useMessageToCreateIncident(messageTitle):
    logging.info("INFO - Current function: " + str(__name__) + ".useMessageToCreateIncident")
    logging.info("INFO - Processing Mail: " + str(messageTitle))
    try:
        matchStringLong = getOutlookTitleMatchStrings()
        logging.info("INFO - matching against: " + str(matchStringLong))
        matches = 0
        for matchString in matchStringLong.split(','):
            if matchString in messageTitle:
                matches += 1
            else:
                pass
        if matches == 0:
            logging.info("INFO - message title DOES NOT requirements for further processing")
            return "notToProcess"
        else:
            logging.info("INFO - message title MATCHES requirements for further processing")
            return "toProcess"
    except Exception as e:
        logging.error("CRITICAL - could not process the title for matching against processing requirements")
        logging.error("CRITICAL - Exception: " + str(e))
        return "couldNotProcessEmailTitle"

def parseWorkOrderNumber(workOrder):
    logging.info("INFO - Current function: " + str(__name__) + ".parseWorkOrderNumber")
    logging.info("INFO - work order line from email: " + str(workOrder))
    try:
        return(workOrder.split('/')[1].strip())
    except Exception as e:
        logging.error("CRITICAL - could not parse the work order line from the email")
        logging.error("CRITICAL - Exception: " + str(e))
        return "couldNotParseWorkOrder"

def transformCompleteUntilDate(completeUntilDateFromMail):
    logging.info("INFO - Current function: " + str(__name__) + ".transformCompleteUntilDate")
    logging.info("INFO - work order line from email: " + str(completeUntilDateFromMail))
    try:
        return(re.sub('-', '.', completeUntilDateFromMail.split(' CEST')[0]))
    except Exception as e:
        logging.error("CRITICAL - could not transform the complete until date from the email")
        logging.error("CRITICAL - Exception: " + str(e))
        return "couldNotTransformCompleteUntilDate"

def parseObn(obnFromMail):
    logging.info("INFO - Current function: " + str(__name__) + ".obnFromMail")
    logging.info("INFO - OBN from email: " + str(obnFromMail))
    try:
        return(re.sub('[A-Za-z]', '', obnFromMail.strip()))
    except Exception as e:
        logging.error("CRITICAL - could not parse the OBN from the email")
        logging.error("CRITICAL - Exception: " + str(e))
        return "couldNotParseObn"