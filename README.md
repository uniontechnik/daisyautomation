# Rules
- Actions and Pages represent the lowest level functions
- Methods in Actions and Pages return TRUE or FALSE only
- Steps implement single or more sets of methods in Actions and Pages
- Steps interpret TRUE / FALSE results and deliver context-specific return values
- Exception: some single action calls to "Actions" may yield context sensitive returns