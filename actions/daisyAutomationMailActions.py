import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import logging
from resources.daisyAutomationConfiguration import *

def sendNotificationMail(evaluateMailToProcessReturn, messageTitle):
    logging.info("INFO - Current function: " + str(__name__) + ".sendAlertMail")
    logging.info("INFO - Argument: " + str(evaluateMailToProcessReturn))

    subject = "Union Technik Daisy Automation: Email Nicht Bearbeitet: Software-Status: " + str(evaluateMailToProcessReturn)
    errorMessageHtml = '''<!DOCTYPE html>
                                        <html>
                                            <head>
                                                <body style="background-color:#ffffff" >
                                                    <div style="height:40%; text-align:center; ">
                                                        <h2 style="color:orange; text-align:center; padding-top:25px;">Mail könnte von Daisy Automation nicht automatisch bearbeitet werden.</h2>
                                                        <h1 style="color:orange; text-align:center; padding-top:25px;">Grund: ''' + str(evaluateMailToProcessReturn) + '''</h1>
                                                        <br>
                                                    </div>
                                                    <br>
                                                    <div style="text-align:center;">
                                                        <p><strong>Betroffene Mail Header: ''' + messageTitle + '''</strong></p>
                                                    </div>
                                                    <div style="text-align:center;">
                                                        <p><strong>Betroffene Mail wurde in "Daisy nicht bearbeitet" Ordner verschoben. Bitte manuell nachbearbeiten.</strong></p>
                                                    </div>
                                                </body>
                                                <meta charset="UTF-8">
                                            </head>
                                        </html>       
                                </html>'''
    messagePlain = 'test'

    try:
        msg = MIMEMultipart('alternative')
        msg['From'] = getEmailSenderEmail()
        msg['To'] = getEmailMainReceiver()
        msg['Cc'] = getEmailCcReceiver()

        msg['Subject'] = subject

        msg.attach(MIMEText(messagePlain, 'plain'))
        msg.attach(MIMEText(errorMessageHtml, 'html'))
        objServer = smtplib.SMTP(getEmailSenderSmtp(), getEmailSenderPort())
        objServer.starttls()
        objServer.login(getEmailSenderEmail(), getEmailSenderPassword())
        sclMessageText = msg.as_string()
        objServer.sendmail(getEmailSenderEmail(), getEmailReceiverList().split(','), sclMessageText)
        objServer.quit()

        logging.info("INFO - Notifcation Email successfully sent")
    except Exception as e:
        logging.error("ERROR - Notifcation Email could not be sent")
        logging.error("ERROR - Exception: " + str(e))


def sendAlertMail(message):
    logging.info("INFO - Current function: " + str(__name__) + ".sendAlertMail")
    logging.info("INFO - Argument: " + str(message))

    subject = "Union Technik Daisy Automation: Kritischer Software-Status: " + str(message)
    errorMessageHtml = '''<!DOCTYPE html>
                                    <html>
                                        <head>
                                            <body style="background-color:#ffffff" >
                                                <div style="height:40%; text-align:center; ">
                                                    <h2 style="color:red; text-align:center; padding-top:25px;">Die Daisy Automation Software wegen kritischer Fehlermeldungen abgeschaltet!</h2>
                                                    <h1 style="color:red; text-align:center; padding-top:25px;">''' + str(message) + '''</h1>
                                                    <br>
                                                </div>
                                                <br>
                                                <div style="text-align:center;">
                                                    <p><strong>Bitte beheben Sie den Fehler und starten die Software neu.</strong></p>
                                                </div>
                                            </body>
                                            <meta charset="UTF-8">
                                        </head>
                                    </html>       
                            </html>'''
    messagePlain = 'test'

    try:
        msg = MIMEMultipart('alternative')
        msg['From'] = getEmailSenderEmail()
        msg['To'] = getEmailMainReceiver()
        msg['Cc'] = getEmailCcReceiver()

        msg['Subject'] = subject

        msg.attach(MIMEText(messagePlain, 'plain'))
        msg.attach(MIMEText(errorMessageHtml, 'html'))
        objServer = smtplib.SMTP(getEmailSenderSmtp(), getEmailSenderPort())
        objServer.starttls()
        objServer.login(getEmailSenderEmail(), getEmailSenderPassword())
        sclMessageText = msg.as_string()
        objServer.sendmail(getEmailSenderEmail(), getEmailReceiverList().split(','), sclMessageText)
        objServer.quit()

        logging.info("INFO - Alert Email successfully sent")
    except Exception as e:
        logging.error("ERROR - Alert Email could not be sent")
        logging.error("ERROR - Exception: " + str(e))