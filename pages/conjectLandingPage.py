import logging

from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.support.expected_conditions import presence_of_element_located
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.common.by import By
import time
from resources.daisyAutomationConfiguration import *

delay = 10
mainMenuExpandCollapse="//div[@id='menudiv']//ul[@class='mainmenu']"

def expandMainMenu(conjectDriver):
    logging.info("INFO - Current function: " + str(__name__) + ".expandMainMenu")
    try:
        WebDriverWait(conjectDriver, delay).until(presence_of_element_located((By.XPATH, mainMenuExpandCollapse))).click()
        logging.info("INFO - Successfully expanded main menu")
        return True
    except Exception as e:
        logging.error("CRITICAL - Could not expand main menu")
        logging.error("CRITICAL - Exception: " + str(e))
        return False

def selectMenuItem(conjectDriver, item):
    logging.info("INFO - Current function: " + str(__name__) + ".selectMenuItem")
    logging.info("INFO - Argument item: " + str(item))
    try:
        elementXpath = "//a[contains(text(),'" + item + "')]"
        WebDriverWait(conjectDriver, delay).until(presence_of_element_located((By.XPATH, elementXpath))).click()
        logging.info("INFO - Successfully selected: " + str(item))
        return True
    except Exception as e:
        logging.error("CRITICAL - Could not select: " + str(item))
        logging.error("CRITICAL - Exception: " + str(e))
        return False
