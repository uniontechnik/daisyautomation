import logging

from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.support.expected_conditions import presence_of_element_located
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.common.action_chains import ActionChains
import time
from resources.daisyAutomationConfiguration import *

delay = 10

firstMessage = "(//div[@data-app-section='MessageList']//div[contains(@class,'customScrollBar')]//div[@data-animatable])[2]"
messagesList = "//div[@data-app-section='MessageList']//div[contains(@class,'customScrollBar')]//div[@data-animatable]"

def openFirstMail(outlookDriver):
    logging.info("INFO - Current function: " + str(__name__) + ".findFirstMail")
    try:
        action = ActionChains(outlookDriver)
        element = WebDriverWait(outlookDriver, delay).until(presence_of_element_located((By.XPATH, firstMessage)))
        action.double_click(on_element=element)
        action.perform()
        logging.info("INFO - Successfully opened first email")
        return True
    except Exception as e:
        logging.error("CRITICAL - Could not complete login")
        logging.error("CRITICAL - Exception: " + str(e))
        return False

def openContextMenu(outlookDriver):
    logging.info("INFO - Current function: " + str(__name__) + ".openContextMenu")
    try:
        action = ActionChains(outlookDriver)
        element = WebDriverWait(outlookDriver, delay).until(presence_of_element_located((By.XPATH, firstMessage)))
        action.context_click(on_element=element)
        action.perform()
        time.sleep(3)
        logging.info("INFO - Successfully right-clicked first email")
        return True
    except Exception as e:
        logging.error("CRITICAL - Could not right-click")
        logging.error("CRITICAL - Exception: " + str(e))
        return False

def switchToFolder(outlookDriver, folder):
    logging.info("INFO - Current function: " + str(__name__) + ".switchToFolder")
    logging.info("INFO - Argument: " + str(folder))
    try:
        folderXpath = "//div[@data-app-section='NavigationPane']//div[@role='tree']//div[contains(@title,'" + folder + "')]"
        WebDriverWait(outlookDriver, delay).until(presence_of_element_located((By.XPATH, folderXpath))).click()
        logging.info("INFO - Successfully switched to folder: " + str(folder))
        return True
    except Exception as e:
        logging.error("ERROR - Could not switch to folder: " + str(folder))
        logging.error("ERROR - Exception: " + str(e))
        return False

def isAMailToProcessFound(outlookDriver):
    logging.info("INFO - Current function: " + str(__name__) + ".isFolderEmpty")
    try:
        element = WebDriverWait(outlookDriver, delay).until(presence_of_element_located((By.XPATH, firstMessage)))
        logging.info("INFO - There are mails to Process")
        return True
    except Exception as e:
        logging.info("INFO - there are no mails to process")
        logging.error("ERROR - Exception: " + str(e))
        return False
